<?php
/**
 * @file
 * uuid_node hooks on behalf of the taxonomy module.
 */

/**
 * Implements hook_uuid_node_features_export_render().
 */
function taxonomy_uuid_node_features_export_alter(&$export, &$pipe, $node) {
  if (!empty($node->taxonomy)) {
    foreach ($node->taxonomy as $term) {
      $vocabulary = taxonomy_vocabulary_load($term->vid);
      $voc_uuid = $vocabulary->machine_name;
      $pipe['uuid_vocabulary'][$voc_uuid] = $voc_uuid;

      $term_uuid = $term->uuid;
      $pipe['uuid_term'][$term_uuid] = $term_uuid;
    }
  }
}

/**
 * Implements hook_uuid_node_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function taxonomy_uuid_node_features_export_render_alter(&$export, &$node, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'node', 'taxonomy_term_reference');
  uuid_features_set_uuid_references($fields, 'taxonomy_term');
}

/**
 * Implements hook_uuid_user_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function taxonomy_uuid_user_features_export_render_alter(&$export, &$user, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'user', 'taxonomy_term_reference');
  uuid_features_set_uuid_references($fields, 'taxonomy_term');
}


/**
 * Implements hook_uuid_node_features_rebuild_alter().
 */
function taxonomy_uuid_node_features_rebuild_alter(&$node, $module) {
  $fields = uuid_features_get_field_items_iterator($node, 'node', 'taxonomy_term_reference');
  uuid_features_fetch_uuid_references($fields, 'taxonomy_term', array(
    'tid' => 'tid',
  ));
}

/**
 * Implements hook_uuid_user_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function taxonomy_uuid_user_features_rebuild_alter(&$user, $module) {
  $fields = uuid_features_get_field_items_iterator($user, 'user', 'taxonomy_term_reference');
  uuid_features_fetch_uuid_references($fields, 'taxonomy_term', array(
    'tid' => 'tid',
  ));
}
