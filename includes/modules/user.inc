<?php

/**
 * Implements hook_uuid_node_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function user_uuid_node_features_export_render_alter(&$export, &$node, $module) {
  $uuid = reset(entity_get_uuid_by_id('user', array($node->uid)));
  if ($uuid) {
    $export->user_uuid = $uuid;
  }
}

/**
 * Implements hook_uuid_node_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function user_uuid_node_features_rebuild_alter(&$node, $module) {
  if (isset($node->user_uuid) && uuid_is_valid($node->user_uuid)) {
    $uid = reset(entity_get_id_by_uuid('user', array($node->user_uuid)));
    if ($uid) {
      $node->uid = $uid;
    } else {
      $node->uid = 1;
    }
  } else {
    $node->uid = 1;
  }
}
