<?php


/**
 * Implements hook_uuid_node_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function field_collection_uuid_node_features_export_render_alter(&$export, &$node, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'node', 'field_collection');
  uuid_features_set_uuid_references($fields, 'field_collection_item');
}

/**
 * Implements hook_uuid_user_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function field_collection_uuid_user_features_export_render_alter(&$export, &$user, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'user', 'field_collection');
  uuid_features_set_uuid_references($fields, 'field_collection_item');
}

/**
 * Implements hook_uuid_node_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function field_collection_uuid_node_features_rebuild_alter(&$node, $module) {
  $fields = uuid_features_get_field_items_iterator($node, 'node', 'field_collection');
  uuid_features_fetch_uuid_references($fields, 'field_collection_item', array(
    'item_id' => 'value',
    'revision_id' => 'revision_id',
  ));
}

/**
 * Implements hook_uuid_user_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function field_collection_uuid_user_features_rebuild_alter(&$user, $module) {
  $fields = uuid_features_get_field_items_iterator($user, 'user', 'field_collection');
  uuid_features_fetch_uuid_references($fields, 'field_collection_item', array(
    'item_id' => 'value',
    'revision_id' => 'revision_id',
  ));
}
