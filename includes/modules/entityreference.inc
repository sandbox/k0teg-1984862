<?php

/**
 * Implements hook_uuid_node_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function entityreference_uuid_node_features_export_render_alter(&$export, &$node, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'node', 'entityreference');
  _entity_reference_uuid_entity_features_export_set_uuid_references($fields);
}

/**
 * Implements hook_uuid_user_features_export_render_alter().
 *
 * @param $export
 * @param $node
 * @param $module
 */
function entityreference_uuid_user_features_export_render_alter(&$export, &$user, $module) {
  $fields = uuid_features_get_field_items_iterator($export, 'user', 'entityreference');
  _entity_reference_uuid_entity_features_export_set_uuid_references($fields);
}

/**
 * Implements hook_uuid_node_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function entityreference_uuid_node_features_rebuild_alter(&$node, $module) {
  $fields = uuid_features_get_field_items_iterator($node, 'node', 'entityreference');
  _entity_reference_uuid_entity_features_rebuild_fetch_uuid_references($fields);
}

/**
 * Implements hook_uuid_user_features_rebuild_alter().
 *
 * @param $node
 * @param $module
 */
function entityreference_uuid_user_features_rebuild_alter(&$user, $module) {
  $fields = uuid_features_get_field_items_iterator($user, 'user', 'entityreference');
  _entity_reference_uuid_entity_features_rebuild_fetch_uuid_references($fields);
}

/**
 * Helper function to fetch uuid references in case of different fields types.
 *
 * @param $fields
 */
function _entity_reference_uuid_entity_features_rebuild_fetch_uuid_references(&$fields) {
  // Entityreference fields may have different targets (nodes, users, etc.).
  // So we process them one by one.
  foreach ($fields as $field_name => $field) {
    $field_info = field_info_field($field_name);
    $entity_info = entity_get_info($field_info['settings']['target_type']);

    // uuid_features_fetch_uuid_references accepts only references to array as the first parameter.
    $tmp_fields = array(
      $field_name => &$field,
    );

    uuid_features_fetch_uuid_references($tmp_fields, $field_info['settings']['target_type'], array(
      $entity_info['entity keys']['id'] => 'target_id',
    ));
  }
}

/**
 * Helper function to set uuid references in case of different fields types.
 *
 * @param $fields
 */
function _entity_reference_uuid_entity_features_export_set_uuid_references(&$fields) {
  // Entityreference fields may have different targets (nodes, users, etc.).
  // So we process them one by one.
  foreach ($fields as $field_name => &$field) {
    $field_info = field_info_field($field_name);

    // uuid_features_fetch_uuid_references accepts only references to array as the first parameter.
    $tmp_fields = array(
      $field_name => &$field,
    );

    uuid_features_set_uuid_references($tmp_fields, $field_info['settings']['target_type']);
  }
}
